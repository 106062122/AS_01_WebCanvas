function clearPad(){
    var canvas=document.querySelector('canvas');
    var ctx=canvas.getContext("2d");
    ctx.clearRect(0,0,canvas.width,canvas.height);
}